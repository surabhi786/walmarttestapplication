package com.example.surabhi.walmarttestapp.Product;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.surabhi.walmarttestapp.Model.Product;
import com.example.surabhi.walmarttestapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by surabhi on 9/15/17.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    private List<Product> data;
    private Context context;
    private ProductAdapterListener listener;
    private static final int NEXT_PAGE_THRESHOLD = 5;

    public ProductListAdapter(List<Product> data, ProductListFragment listener) {
        this.data = data;
        this.context = listener.getContext();
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_item_view, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Product product = data.get(position);
        if (product == null) {
            return;
        }
        Picasso.with(context).load(product.getProductImage()).into(holder.imageView);
        holder.priceTextView.setText(product.getPrice());
        holder.productTitleView.setText(product.getProductName());
        holder.productRatingView.setRating(product.getReviewRating());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onProductSelected(position);
            }
        });

        if (getItemCount() - position < NEXT_PAGE_THRESHOLD) {
            listener.onNextPage();
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public interface ProductAdapterListener {
        void onNextPage();

        void onProductSelected(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_image_view)
        ImageView imageView;
        @BindView(R.id.product_price)
        TextView priceTextView;
        @BindView(R.id.product_title)
        TextView productTitleView;
        @BindView(R.id.product_rating)
        RatingBar productRatingView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
