package com.example.surabhi.walmarttestapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by surabhi on 9/15/17.
 */

public class ResponseWrapper {
    @SerializedName("products")
    private ArrayList<Product> products;

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
}
