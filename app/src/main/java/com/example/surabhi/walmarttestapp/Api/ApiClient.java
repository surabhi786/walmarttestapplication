package com.example.surabhi.walmarttestapp.Api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by surabhi on 9/15/17.
 */

public class ApiClient {
    public static final String BASE_URL = "https://walmartlabs-test.appspot.com/_ah/api/walmart/v1/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
