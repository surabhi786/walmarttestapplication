package com.example.surabhi.walmarttestapp.Api;

import com.example.surabhi.walmarttestapp.Model.ResponseWrapper;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by surabhi on 9/15/17.
 */

public interface ApiInterface {

    @GET("walmartproducts/{apiKey}/{pageNumber}/{pageSize}")
    Call<ResponseWrapper> getProducts(@Path("apiKey") String apiKey, @Path("pageNumber") int pageNumber, @Path("pageSize") int pageSize);
}