package com.example.surabhi.walmarttestapp.ProductDetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.surabhi.walmarttestapp.Model.Product;
import com.example.surabhi.walmarttestapp.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductDetailFragment extends Fragment {

    @BindView(R.id.product_title)
    TextView productTitle;
    @BindView(R.id.product_rating)
    RatingBar productRatings;
    @BindView(R.id.product_short_details)
    TextView productShortDesc;
    @BindView(R.id.product_long_details)
    TextView productLongDesc;
    @BindView(R.id.product_image_view)
    ImageView productImageView;
    @BindView(R.id.product_price)
    TextView productPrice;
    @BindView(R.id.product_reviews)
    TextView productReviews;

    private Product product;
    private static final String KEY_PRODUCT = "product";

    public static ProductDetailFragment newInstance(Product product) {
        ProductDetailFragment fragment = new ProductDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_PRODUCT, product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        product = getArguments().getParcelable(KEY_PRODUCT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_detail_fragment, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.item_detail));
        setHasOptionsMenu(true);
        if (product != null) {
            productTitle.setText(product.getProductName());
            productRatings.setRating(product.getReviewRating());
            Picasso.with(getActivity()).load(product.getProductImage()).into(productImageView);
            productShortDesc.setText(fromHtml(product.getShortDescription()));
            productPrice.setText(product.getPrice());
            productLongDesc.setText(fromHtml(product.getLongDescription()));
            productReviews.setText(String.valueOf(product.getReviewCount()));
        }
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.product_detail_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.share:
                onShare();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onShare() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getShareString());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result = null;
        if (html != null) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
            } else {
                result = Html.fromHtml(html);
            }
        }
        return result;
    }

    private String getShareString() {
        return getResources().getString(R.string.share_string, product.getProductName(), product.getPrice());
    }
}
