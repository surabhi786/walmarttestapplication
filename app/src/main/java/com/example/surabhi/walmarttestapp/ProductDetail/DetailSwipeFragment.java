package com.example.surabhi.walmarttestapp.ProductDetail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.surabhi.walmarttestapp.Data.DataManager;
import com.example.surabhi.walmarttestapp.Model.Product;
import com.example.surabhi.walmarttestapp.Product.ProductListFragment;
import com.example.surabhi.walmarttestapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by surabhi on 9/15/17.
 */

public class DetailSwipeFragment extends Fragment {

    @BindView(R.id.pager)
    ViewPager pager;

    private PagerAdapter pagerAdapter;
    private List<Product> products;
    private int position;

    private static final String KEY_POSITION = "position";

    public static DetailSwipeFragment newInstance(int position) {
        DetailSwipeFragment fragment = new DetailSwipeFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_swipe_fragment, container, false);
        ButterKnife.bind(this, view);
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(position, true);
        return view;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        products = DataManager.getInstance().getProducts();
        position = getArguments().getInt(KEY_POSITION);
        pagerAdapter = new ProductDetailPagerAdapter(getActivity().getSupportFragmentManager());
    }

    private class ProductDetailPagerAdapter extends FragmentStatePagerAdapter {

        public ProductDetailPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ProductDetailFragment.newInstance(products.get(position));
        }

        @Override
        public int getCount() {
            return products == null ? 0 : products.size();
        }
    }
}
