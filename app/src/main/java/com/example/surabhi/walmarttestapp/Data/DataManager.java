package com.example.surabhi.walmarttestapp.Data;

import com.example.surabhi.walmarttestapp.Model.Product;

import java.util.ArrayList;

/**
 * Created by surabhi on 9/17/17.
 */

public class DataManager {

    private static volatile DataManager sSoleInstance = new DataManager();

    private DataManager(){}

    private ArrayList<Product> products = new ArrayList<>();

    public static DataManager getInstance() {
        return sSoleInstance;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products, boolean reset) {
        if (reset) {
            this.products.clear();
        }
        this.products.addAll(products);
    }
}