package com.example.surabhi.walmarttestapp.Product;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.surabhi.walmarttestapp.Api.ApiClient;
import com.example.surabhi.walmarttestapp.Api.ApiInterface;
import com.example.surabhi.walmarttestapp.Data.DataManager;
import com.example.surabhi.walmarttestapp.Model.Product;
import com.example.surabhi.walmarttestapp.Model.ResponseWrapper;
import com.example.surabhi.walmarttestapp.ProductDetail.DetailSwipeFragment;
import com.example.surabhi.walmarttestapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListFragment extends Fragment implements ProductListAdapter.ProductAdapterListener {

    private static final String API_KEY = "e0a4274f-45b6-405b-839e-1096222be4fc";
    private LinearLayoutManager layoutManager;
    private ProductListAdapter adapter;
    private static final String TAG = "ProductListFragment";
    private int pageNumber = 1;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.spinner)
    ProgressBar spinner;

    private static final int pageSize = 30;

    public static ProductListFragment newInstance() {
        return new ProductListFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.app_name));
        toolbar.getMenu().clear();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (API_KEY.isEmpty()) {
            Toast.makeText(getActivity().getBaseContext(), "Please add API key", Toast.LENGTH_LONG);
            return;
        }
        adapter = new ProductListAdapter(DataManager.getInstance().getProducts(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_list_fragment, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        fetchData();
        return view;
    }

    private void fetchData() {
        spinner.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseWrapper> call = apiService.getProducts(API_KEY, pageNumber, pageSize);
        call.enqueue(new Callback<ResponseWrapper>() {
            @Override
            public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                spinner.setVisibility(View.GONE);
                ResponseWrapper rm = response.body();
                if (rm != null) {
                    DataManager.getInstance().setProducts(rm.getProducts(), pageNumber == 1);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                spinner.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getResources().getString(R.string.error_msg_default),
                        Toast.LENGTH_LONG).show();
                Log.d(TAG, "API request failed: " + t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onNextPage() {
        pageNumber++;
        fetchData();
    }

    @Override
    public void onProductSelected(int position) {
        DetailSwipeFragment detailSwipeFragment = DetailSwipeFragment.newInstance(position);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, detailSwipeFragment, TAG)
                .addToBackStack(null)
                .commit();
    }
}
